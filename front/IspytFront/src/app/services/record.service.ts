import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http'
import { Observable, tap} from 'rxjs';
import { IRecord } from './../models/record';
import { NewRecord } from './../models/newRecord';

@Injectable({
  providedIn: 'root'
})
export class RecordService {
  constructor(private http: HttpClient) { }

  records: IRecord[] = []


  getAll(): Observable<IRecord[]>
  {
      return this.http.get<IRecord[]>('https://localhost:7071/api/Record/records').pipe(
          tap((records) => {this.records = records;})
      )
  }

  addRecord(record : NewRecord):Observable<IRecord[]>
  {
      return this.http.post<IRecord[]>('https://localhost:7071/api/Record/newRecord', record)
  }

  
  updateRecord(record : IRecord):Observable<IRecord[]>
  {
      return this.http.put<IRecord[]>('https://localhost:7071/api/Record/record', record)
  }

  deleteRecord(id : string):Observable<IRecord[]>
  {
      return this.http.delete<IRecord[]>('https://localhost:7071/api/Record/records/'+id)
  }
}
