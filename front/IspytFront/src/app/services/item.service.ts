import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http'
import { Item } from 'src/app/models/item';
import { Observable, tap} from 'rxjs';
import { NewItem } from 'src/app/models/newItem';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  constructor(private http: HttpClient) { }

  items: Item[] = []

  getAll(): Observable<Item[]>
  {
      return this.http.get<Item[]>('https://localhost:7071/api/Item/items').pipe(
          tap((items) => {this.items = items;})
      )
  }

  addItem(item : NewItem):Observable<Item[]>
  {
      return this.http.post<Item[]>('https://localhost:7071/api/Item/newItem', item)
  }

  updateItem(item : Item):Observable<Item[]>
  {
      return this.http.post<Item[]>('https://localhost:7071/api/Item/item', item)
  }
  
  deleteItem(id : string):Observable<Item[]>
  {
      return this.http.delete<Item[]>('https://localhost:7071/api/Item/items/'+id)
  }
}
