import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  isVisible$ = new BehaviorSubject<boolean>(false)
  isDeleteVisible$ = new BehaviorSubject<boolean>(false)

  open() {
    this.isVisible$.next(true)
  }

  openDeleteModal() {
    this.isDeleteVisible$.next(true)
  }
  
  close() {
    this.isVisible$.next(false)
    this.isDeleteVisible$.next(false)
  }
}
