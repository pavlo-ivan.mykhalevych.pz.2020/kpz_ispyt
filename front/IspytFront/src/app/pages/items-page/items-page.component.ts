import { Component } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { ItemService } from './../../services/item.service';

@Component({
  selector: 'app-items-page',
  templateUrl: './items-page.component.html',
  styleUrls: ['./items-page.component.css']
})
export class ItemsPageComponent {
  title = 'Items list';
  id = "";

  constructor(
    public itemService: ItemService,
    public modalService: ModalService
  ) {
  }

  ngOnInit(): void {
    this.itemService.getAll().subscribe(); 
  }
}
