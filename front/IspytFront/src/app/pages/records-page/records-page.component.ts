import { Component } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { RecordService } from 'src/app/services/record.service';

@Component({
  selector: 'app-records-page',
  templateUrl: './records-page.component.html',
  styleUrls: ['./records-page.component.css']
})
export class RecordsPageComponent {
  title = 'History';
  id = "";

  constructor(
    public recordsService: RecordService,
    public modalService: ModalService
  ) {
  }

  ngOnInit(): void {
    this.recordsService.getAll().subscribe(); 
  }
}
