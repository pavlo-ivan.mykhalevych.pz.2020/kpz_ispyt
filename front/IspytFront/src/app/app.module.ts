import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FilterItemsPipe } from './pipes/filter-items.pipe';
import { FilterRecordsPipe } from './pipes/filter-records.pipe';
import { ItemComponent } from './components/item/item.component';
import { RecordComponent } from './components/record/record.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import { ItemsPageComponent } from './pages/items-page/items-page.component';
import { RecordsPageComponent } from './pages/records-page/records-page.component';
import { ModalComponent } from "./components/modal/modal.component";
import { CreateItemComponent } from './forms/create-item/create-item.component';
import { DeleteItemComponent } from './forms/delete-item/delete-item.component';
import { CreateRecordComponent } from './forms/create-record/create-record.component';
import { DeleteRecordComponent } from './forms/delete-record/delete-record.component';

@NgModule({
    declarations: [
        AppComponent,
        FilterItemsPipe,
        FilterRecordsPipe,
        ModalComponent,
        ItemComponent,
        RecordComponent,
        ItemsPageComponent,
        RecordsPageComponent,
        CreateItemComponent,
        DeleteItemComponent,
        CreateRecordComponent,
        DeleteRecordComponent
    ],
    providers: [],
    bootstrap: [AppComponent],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class AppModule { }
