import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ItemsPageComponent } from './pages/items-page/items-page.component';
import { RecordsPageComponent } from './pages/records-page/records-page.component';

const routes: Routes = [
  { path: 'items', component: ItemsPageComponent},
  { path: 'records', component: RecordsPageComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
