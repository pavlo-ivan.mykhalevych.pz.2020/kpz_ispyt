import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalService } from 'src/app/services/modal.service';
import { RecordService } from 'src/app/services/record.service';

@Component({
  selector: 'app-create-record',
  templateUrl: './create-record.component.html',
  styleUrls: ['./create-record.component.css']
})
export class CreateRecordComponent {
  constructor(
    private recordService: RecordService,
    private modalService: ModalService
  ) {
  }

  form = new FormGroup({
    
    id: new FormControl<number>(0, [
      Validators.required,
    ]),
    surname: new FormControl<string>('', [
      Validators.required,
    ]),
    startUsingYear: new FormControl<number>(0, [
      Validators.required,
    ]),
    endUsingYear: new FormControl<number>(0, [
      Validators.required,
    ]),
    itemId: new FormControl<number>(0, [
      Validators.required,
    ])
  })

  get id() {
    return this.form.controls.id as FormControl
  }
  get surname() {
    return this.form.controls.surname as FormControl
  }
  get startUsingYear() {
    return this.form.controls.startUsingYear as FormControl
  }
  get endUsingYear() {
    return this.form.controls.endUsingYear as FormControl
  }
  get itemId() {
    return this.form.controls.itemId as FormControl
  }

  submit() {
    this.recordService.addRecord({
      surname: this.form.value.surname as string,
      startUsingYear: this.form.value.startUsingYear as number,
      endUsingYear: this.form.value.endUsingYear as number,
      itemId: this.form.value.itemId as number,
    })
    .subscribe(() => {
      this.recordService.getAll()
      this.modalService.close()
      window.location.reload()
    })
  }
}
