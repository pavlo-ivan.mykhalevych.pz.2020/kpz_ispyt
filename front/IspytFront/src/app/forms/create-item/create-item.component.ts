import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalService } from 'src/app/services/modal.service';
import { ItemService } from './../../services/item.service';

@Component({
  selector: 'app-create-item',
  templateUrl: './create-item.component.html',
  styleUrls: ['./create-item.component.css']
})
export class CreateItemComponent {
  constructor(
    private itemService: ItemService,
    private modalService: ModalService
  ) {
  }

  form = new FormGroup({
    name: new FormControl<string>('', [
      Validators.required,
    ]),
    price: new FormControl<number>(0, [
      Validators.required,
    ]),
    releaseYear: new FormControl<number>(0, [
      Validators.required,
    ]),
    room: new FormControl<string>('', [
      Validators.required,
    ]),
    writeOffYear: new FormControl<number>(0, [
      Validators.required
    ])
  })

  get name() {
    return this.form.controls.name as FormControl
  }
  get price() {
    return this.form.controls.price as FormControl
  }
  get releaseYear() {
    return this.form.controls.releaseYear as FormControl
  }
  get room() {
    return this.form.controls.room as FormControl
  }
  get writeOffYear() {
    return this.form.controls.writeOffYear as FormControl
  }

  submit() {
    this.itemService.addItem({
      name: this.form.value.name as string,
      price: this.form.value.price as number,
      releaseYear: this.form.value.releaseYear as number,
      room: this.form.value.room as string,
      writeOffYear: this.form.value.writeOffYear as number,
    }).subscribe(() => {
      this.itemService.getAll()
      this.modalService.close()
      window.location.reload()
    })
  }
}
