import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalService } from 'src/app/services/modal.service';
import { ItemService } from './../../services/item.service';

@Component({
  selector: 'app-delete-item',
  templateUrl: './delete-item.component.html',
  styleUrls: ['./delete-item.component.css']
})
export class DeleteItemComponent {
  constructor(
    private itemService: ItemService,
    private modalService: ModalService
  ) {
  }

  form = new FormGroup({    
    id: new FormControl<string>('', [
      Validators.required,
    ])
  });

  get id() {
    return this.form.controls.id as FormControl
  }

  submit() {
    this.itemService.deleteItem( this.form.value.id as string ).subscribe(() => {
      this.itemService.getAll()
      this.modalService.close()
      window.location.reload()
    })
  }
}
