import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalService } from 'src/app/services/modal.service';
import { RecordService } from 'src/app/services/record.service';

@Component({
  selector: 'app-delete-record',
  templateUrl: './delete-record.component.html',
  styleUrls: ['./delete-record.component.css']
})
export class DeleteRecordComponent {
  constructor(
    private recordService: RecordService,
    private modalService: ModalService
  ) {
  }

  form = new FormGroup({    
    id: new FormControl<string>('', [
      Validators.required,
    ])
  });

  get id() {
    return this.form.controls.id as FormControl
  }

  submit() {
    this.recordService.deleteRecord( this.form.value.id as string ).subscribe(() => {
      this.recordService.getAll()
      this.modalService.close()
      window.location.reload();
    })
  }
}
