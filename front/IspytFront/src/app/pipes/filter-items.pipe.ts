import { Pipe, PipeTransform } from '@angular/core';
import { Item } from '../models/item';

@Pipe({
  name: 'filterItems'
})
export class FilterItemsPipe implements PipeTransform {

  transform(items: Item[], id: string): Item[] {
    if (id == "") return items;
    return items.filter(it => it.id == +id);
  }

}
