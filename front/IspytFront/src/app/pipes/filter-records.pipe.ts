import { Pipe, PipeTransform } from '@angular/core';
import { IRecord } from './../models/record';

@Pipe({
  name: 'filterRecords'
})
export class FilterRecordsPipe implements PipeTransform {

  transform(records: IRecord[], id: string): IRecord[] {
    if (id == "") return records;
    return records.filter(r => r.id == +id);
  }

}
