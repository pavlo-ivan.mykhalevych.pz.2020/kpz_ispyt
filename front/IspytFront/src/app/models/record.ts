export interface IRecord{
    id : number
    surname : string
    startUsingYear : number
    endUsingYear : number
    itemId : number
}