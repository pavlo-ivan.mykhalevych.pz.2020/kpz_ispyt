export interface Item{
    id : number
    name : string
    price : number
    releaseYear : number
    room : string
    writeOffYear : number
}