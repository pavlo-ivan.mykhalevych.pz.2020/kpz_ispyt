export interface NewItem{
    name : string
    price : number
    releaseYear : number
    room : string
    writeOffYear : number
}