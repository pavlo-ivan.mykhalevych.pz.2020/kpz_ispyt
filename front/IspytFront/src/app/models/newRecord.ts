export interface NewRecord{
    surname : string
    startUsingYear : number
    endUsingYear : number
    itemId : number
}