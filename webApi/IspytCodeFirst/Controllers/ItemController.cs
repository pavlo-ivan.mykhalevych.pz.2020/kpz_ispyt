﻿using IspytCodeFirst.DB;
using IspytCodeFirst.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Numerics;

namespace IspytCodeFirst.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        public ItemController()
        {

        }

        /// <summary>
        /// Gets the list of all items.
        /// </summary>
        [HttpGet("items")]
        public async Task<ActionResult<List<Item>>> Get()
        {
            using var dbcontext = new IspytContext();
            return Ok(await dbcontext.Items.ToListAsync());
        }

        /// <summary>
        /// Gets the item.
        /// </summary>
        [HttpGet("items/{id}")]
        public async Task<ActionResult<Item>> Get(int id)
        {
            using var dbcontext = new IspytContext();
            var hero = await dbcontext.Items.FindAsync(id);
            if (hero == null)
                return BadRequest("Item not found.");
            return Ok(hero);
        }

        /// <summary>
        /// Add the item.
        /// </summary>
        [HttpPost("newItem")]
        public async Task<ActionResult<List<Item>>> AddStaff(Item request)
        {
            using var dbcontext = new IspytContext();
            dbcontext.Items.Add(request);
            await dbcontext.SaveChangesAsync();

            return Ok(await dbcontext.Items.ToListAsync());
        }

        /// <summary>
        /// Update the item info.
        /// </summary>
        [HttpPut("item")]
        public async Task<ActionResult<List<Item>>> UpdateStaff(Item request)
        {
            using var dbcontext = new IspytContext();
            var dbObject = await dbcontext.Items.FindAsync(request.Id);
            if (dbObject == null)
                return BadRequest("Item not found.");

            dbObject.Name = request.Name;
            dbObject.Price = request.Price;
            dbObject.ReleaseYear = request.ReleaseYear;
            dbObject.Room = request.Room;
            dbObject.WriteOffYear = request.WriteOffYear;

            await dbcontext.SaveChangesAsync();

            return Ok(await dbcontext.Items.ToListAsync());
        }

        /// <summary>
        /// Delete the item.
        /// </summary>
        [HttpDelete("items/{id}")]
        public async Task<ActionResult<List<Item>>> Delete(int id)
        {
            using var dbcontext = new IspytContext();
            var dbObject = await dbcontext.Items.FindAsync(id);
            if (dbObject == null)
                return BadRequest("Item not found.");

            dbcontext.Items.Remove(dbObject);
            await dbcontext.SaveChangesAsync();

            return Ok(await dbcontext.Items.ToListAsync());
        }
    }
}
