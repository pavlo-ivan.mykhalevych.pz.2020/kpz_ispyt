﻿using IspytCodeFirst.DB;
using IspytCodeFirst.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Numerics;

namespace IspytCodeFirst.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecordController : ControllerBase
    {
        public RecordController()
        {

        }

        /// <summary>
        /// Gets the list of all staff members.
        /// </summary>
        [HttpGet("records")]
        public async Task<ActionResult<List<Record>>> Get()
        {
            using var dbcontext = new IspytContext();
            return Ok(await dbcontext.Records.ToListAsync());
        }

        /// <summary>
        /// Gets the staff member.
        /// </summary>
        [HttpGet("records/{id}")]
        public async Task<ActionResult<Record>> Get(int id)
        {
            using var dbcontext = new IspytContext();
            var hero = await dbcontext.Records.FindAsync(id);
            if (hero == null)
                return BadRequest("Record not found.");
            return Ok(hero);
        }

        /// <summary>
        /// Add the staff member.
        /// </summary>
        [HttpPost("newRecord")]
        public async Task<ActionResult<List<Record>>> AddStaff(Record request)
        {
            using var dbcontext = new IspytContext();
            dbcontext.Records.Add(request);
            await dbcontext.SaveChangesAsync();

            return Ok(await dbcontext.Records.ToListAsync());
        }

        /// <summary>
        /// Update the staff member info.
        /// </summary>
        [HttpPut("record")]
        public async Task<ActionResult<List<Record>>> UpdateStaff(Record request)
        {
            using var dbcontext = new IspytContext();
            var dbObject = await dbcontext.Records.FindAsync(request.Id);
            if (dbObject == null)
                return BadRequest("Record not found.");

            dbObject.Surname = request.Surname;
            dbObject.StartUsingYear = request.StartUsingYear;
            dbObject.EndUsingYear = request.EndUsingYear;
            dbObject.ItemId = request.ItemId;

            await dbcontext.SaveChangesAsync();

            return Ok(await dbcontext.Records.ToListAsync());
        }

        /// <summary>
        /// Delete the staff member.
        /// </summary>
        [HttpDelete("records/{id}")]
        public async Task<ActionResult<List<Record>>> Delete(int id)
        {
            using var dbcontext = new IspytContext();
            var dbObject = await dbcontext.Records.FindAsync(id);
            if (dbObject == null)
                return BadRequest("Record not found.");

            dbcontext.Records.Remove(dbObject);
            await dbcontext.SaveChangesAsync();

            return Ok(await dbcontext.Records.ToListAsync());
        }
    }
}
