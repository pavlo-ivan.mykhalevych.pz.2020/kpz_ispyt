﻿using IspytCodeFirst.Models;
using Microsoft.EntityFrameworkCore;

namespace IspytCodeFirst.DB
{
    public class IspytContext : DbContext
    {
        public IspytContext()
        {
        }

        public IspytContext(DbContextOptions options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Data Source=TSKT-LT-028;Initial Catalog=Ispyt;Integrated Security=True;TrustServerCertificate=True;");
            }
        }
        public DbSet<Item> Items { get; set; }
        public DbSet<Record> Records { get; set; }
    }
}
