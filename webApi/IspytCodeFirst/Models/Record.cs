﻿using System.ComponentModel.DataAnnotations;

namespace IspytCodeFirst.Models
{
    public class Record
    {
        [Key]
        public int Id { get; set; }
        public string Surname { get; set; }
        public int StartUsingYear { get; set; }
        public int EndUsingYear { get; set; }
        public int ItemId { get; set; }
    }
}
