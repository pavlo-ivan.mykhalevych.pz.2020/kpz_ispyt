﻿using System.ComponentModel.DataAnnotations;

namespace IspytCodeFirst.Models
{
    public class Item
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
        public int ReleaseYear { get; set; }
        public string Room { get; set; }
        public int WriteOffYear { get; set; }
    }
}
